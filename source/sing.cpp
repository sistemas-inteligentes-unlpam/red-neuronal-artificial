#include "Sing.hpp"

double Sing::calculate(double input){
  if(input > 0) {return 1;}
  else if(input < 0) {return -1;}
  else {return 0;}
}

double Sing::derivative(double input){
  return 1.0;
}
