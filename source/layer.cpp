#include "Layer.hpp"

Layer::Layer(int num_units, int prev_num_units, int bias, int fn){
  if(bias !=0){
    i_size = prev_num_units + 1;
    theta = bias;
  }
  else{
    i_size = prev_num_units;
  }
  l_size = num_units;
  deltas = (double*) calloc(l_size, sizeof(double));
  inputs = (double*) calloc(i_size, sizeof(double));
  outputs = (double*) calloc(l_size, sizeof(double));
  neuron = (Neuron**) malloc(l_size * sizeof(Neuron));
  for(int i=0; i<l_size; i++){
    neuron[i] = new Neuron(i_size, fn);
  }
}

int Layer::get_l_size(){
  return l_size;
}

double* Layer::get_inputs(){
  return inputs;
}

double* Layer::get_outputs(){
  return outputs;
}

Neuron* Layer::get_neuron(int i){
  return neuron[i];
}

double* Layer::get_deltas(){
  return deltas;
}

double Layer::get_delta(int i){
  return deltas[i];
}

double* Layer::add_bias(double* in){
  inputs[0] = theta;
  for(int i=1; i<i_size; i++){
    inputs[i] = in[i-1];
  }
  return inputs;
}

double* Layer::evaluate(double *in){
  if(theta != 0){ inputs = add_bias(in);}
  else{ inputs = in; }
  //cout << "layer.evaluate()" << endl;
  //print_input();
  for(int i=0; i<l_size; i++){
    outputs[i] = neuron[i]->activate(inputs);
  }
  //print_output();
  return outputs;
}

void Layer::calculate_deltas_output_layer(double *desired, double eta){
  //cout << "layer.calculate_deltas_output_layer()" << endl;
  //print_input();
  for(int i=0; i<l_size; i++){
    deltas[i] = (desired[i] - outputs[i]) * neuron[i]->derivative(inputs);
    neuron[i]->calculate_delta_w_output_layer(eta, deltas[i], inputs);
  }
  //print_deltas();
}

void Layer::calculate_deltas(Layer *sup_layer, double eta){
  //cout << "layer.calculate_deltas()" << endl;
  //print_input();
  for(int j=0; j<l_size; j++){
    for(int i=0; i<sup_layer->get_l_size(); i++){
      deltas[j] += sup_layer->get_delta(i)
                    * sup_layer->get_neuron(i)->get_weight(j);
      deltas[j] = deltas[j] * neuron[j]->derivative(inputs);     
    }
    neuron[j]->calculate_delta_w(eta, deltas[j], inputs);
  }
  //print_deltas();
}

void Layer::print_l_size(){
  cout << "LAYER SIZE: " << l_size << endl;
}

void Layer::print_bias(){
  cout << "BIAS: " << theta << endl;
}

void Layer::print_input(){
  cout << "=================== INPUTS ===================" << endl;
  for(int i=0; i<i_size; i++){
    cout << "in[" << i << "]:" << inputs[i] <<  endl;
  }
  cout << "=============================================="<< endl;
}

void Layer::print_output(){
  cout << "=================== OUTPUTS ===================" << endl;
  for(int i=0; i<l_size; i++){
    cout << "out[" << i << "]:" << outputs[i] <<  endl;
  }
  cout << "=============================================="<< endl;
}

void Layer::print_deltas(){
  cout << "=================== DELTAS ===================" << endl;
  for(int i=0; i<l_size; i++){
    cout << "deltas[" << i << "]:" << deltas[i] <<  endl;
  }
  cout << "=============================================="<< endl;
}

void Layer::print_neurons(){
  Neuron *neuron;
  for(int i=0; i<l_size; i++){
    cout << "=================== NEURON " << i << " ===================" << endl;
    neuron = get_neuron(i);
    neuron->summarize_neuron();
  }
  cout << "=============================================="<< endl;
}

void Layer::summarize_layer(int k){
  cout << "===================== CAPA: " << k << " ===========================" << endl;
  print_l_size();
  print_input();
  print_output();
  print_deltas();
  print_neurons();
  cout << "====================== FIN CAPA " << k << " ==================================" << endl;
}
