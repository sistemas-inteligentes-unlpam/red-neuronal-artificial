#include "MultilayerPerceptron.hpp"

MultilayerPerceptron::MultilayerPerceptron(int num_layers, double lr, int* num_neurons, int* fn){
  srand(time(NULL));
  rn_size = num_layers;
  eta = lr;
  layers = (Layer**) malloc(rn_size*sizeof(Layer));
  for(int k=0; k<rn_size; k++){
    layers[k] = new Layer(num_neurons[k+1], num_neurons[k], -1, fn[k]);// ante último parametro bias
  }
}

double MultilayerPerceptron::quadraticError(int mu, double *desired, double *output){
  double mse = 0.0;
  for(int i=0; i<layers[rn_size -1]->get_l_size(); i++){
    //cout << "desired: " << desired[i] << endl;
    //cout << "output: " << output[i] << endl;
    //cin.get();
    mse += (desired[i] - output[i]) * (desired[i] - output[i]);
  }
  cout <<"mse: " << mse << endl;
  //return mse / mu;
  return mse;
}

void MultilayerPerceptron::train(int epoch, int mu, double **desired, double **inputs){
  double *outputs;
  for(int i=0; i<epoch; i++){
    shuffle(inputs, mu);
    double mse = 0.0;
    for(int j=0; j<mu; j++){
      outputs = activate(inputs[j]);
      layers[0]->print_input();
      layers[rn_size-1]->print_output();
      mse += quadraticError(mu, desired[j], outputs);
      backpropagation(desired[j], outputs);
      //take_snapshoot();
    }
    cout << "ERROR: " << mse / mu << endl;
  }
  //summarize_network();
}

double* MultilayerPerceptron::activate(double *inputs){
  double *outputs;
  for(int k=0; k<rn_size; k++){
    outputs = layers[k]->evaluate(inputs);
    inputs = outputs;
  }
  return outputs;
}

void MultilayerPerceptron::test(int mu, double **inputs){
  shuffle(inputs, mu);
  for(int j=0; j<mu; j++){
    activate(inputs[j]);
    layers[0]->print_input();
    layers[rn_size-1]->print_output();
    //take_snapshoot();
  }
  /*for(int k=0; k<rn_size; k++){    
      outputs = layers[k]->evaluate(inputs[j]);
      outputs = inputs;    
  }*/
}

void MultilayerPerceptron::shuffle(double **inputs, int size){
  int r1 = 0;
  int r2 = 0;
  srand(time(NULL));  
  for(int i=0; i<size*2; i++){
    r1 = rand() % size;
    r2 = rand() % size;
    interchange(&inputs[r1], &inputs[r2]);
  }
}

void MultilayerPerceptron::interchange (double **pa, double **pb){
  double *aux = (double*) malloc(2*sizeof(double));
  aux = *pa;
  *pa = *pb;
  *pb = aux;
}

void MultilayerPerceptron::backpropagation(double* desired, double* outputs){
  layers[rn_size-1]->calculate_deltas_output_layer(desired, eta);
  for(int k=rn_size-2; k >= 0; k--){
    layers[k]->calculate_deltas(layers[k+1], eta);;
  }
}

void MultilayerPerceptron::print_rn_size(){
  cout << "********** NRO DE CAPAS EN LA RED : "<< rn_size << " ********** " << endl;
  cout << "************************************************" << endl;
}

void MultilayerPerceptron::print_eta(){
  cout << "********** TASA DE APRENDIZAJE : "<< eta << " ***********" << endl;
  cout << "************************************************" << endl;
}

void MultilayerPerceptron::summarize_network(){
  cout << "******************** NEURAL NETWORK ARCHITECTURE ********************" << endl;
  print_rn_size();
  print_eta();
  for(int k=0; k<rn_size; k++){
    layers[k]->summarize_layer(k);
  }
cout << "***********************************************************************" << endl;
}

void MultilayerPerceptron::take_snapshoot(){
  summarize_network();
  cin.get();
}
