#include "Sigmoid.hpp"

double Sigmoid::calculate(double input){
  return 1 / (1 + exp(-input));
}

double Sigmoid::derivative(double input){
  return exp(-input) / ((1 + exp(-input)) * (1 + exp(-input)));
}
