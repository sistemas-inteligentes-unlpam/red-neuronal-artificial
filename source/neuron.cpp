#include "Neuron.hpp"

Neuron::Neuron(int input_size, int fn){
  w_size = input_size;
  af =  new ActivationFunction(fn);
  weights = (double*) malloc(w_size*sizeof(double));
  delta_w = (double*) calloc(w_size, sizeof(double));
  for(int j=0; j< w_size; j++){
    weights[j] = (double) rand() / RAND_MAX;
  }
}

double* Neuron::get_weights(){
  return weights;
}

double Neuron::get_weight(int i){
  return weights[i];
}

int Neuron::get_w_size(){
  return w_size;
}

double Neuron::sum(double *input){
  double res = 0.0;
  for(int i=0; i < w_size; i++){
    res += input[i] * weights[i];
  }
  return res;
}

double Neuron::activate(double *input){
  //cout << "neuron.activate()" << endl;
  double output = 0.0;
  output = sum(input);
  //output = 1 / (1 + exp(-output));
  output = af->calculate(output);
  return output;
}

double Neuron::derivative(double *input){
  //cout << "neuron.derivative()" << endl;
  double output = 0.0;
  output = sum(input);
  //output = exp(-output) / ((1 + exp(-output)) * (1 + exp(-output)));
  output = af->derivative(output);
  return output;
}

//unificar con calculate_delta_w
void Neuron::calculate_delta_w_output_layer(double eta, double delta, double *inputs){
  for(int j=0; j<w_size; j++){
    delta_w[j] = eta * delta * inputs[j];
  }
  update_weights();
}

void Neuron::calculate_delta_w(double eta, double delta, double* inputs){
  for(int i=0; i<w_size; i++){
    //delta_w[i] = eta * delta * derivative(inputs) * inputs[i];
    delta_w[i] = eta * delta * inputs[i];
  }
  update_weights();
}

double Neuron::deltas_times_weights(double *deltas){
  double res = 0.0;
  for(int i=0; i<w_size; i++){
    res += deltas[i] * weights[i];
  }
  return res;
}

void Neuron::update_weights(){
  for(int i=0; i<w_size; i++){
    weights[i] += delta_w[i];
  }
}

void Neuron::summarize_neuron(){
  cout << "w_size: " << w_size << endl;
  print_weights();
  print_delta_w();
}

void Neuron::print_weights(){
  cout << "---------- WEIGHTS ----------------" << endl;
  for(int j=0; j<w_size; j++){
    cout << "w[" << j << "]:" << weights[j] <<  endl;
  }
  cout << "-----------------------------------"<< endl;
}

void Neuron::print_delta_w(){
  cout << "---------- DELTAS W ----------------" << endl;
  for(int j=0; j<w_size; j++){
    cout << "dw[" << j << "]:" << delta_w[j] <<  endl;
  }
  cout << "-----------------------------------"<< endl;
}
