#ifndef SIGMOID_HPP
#define SIGMOID_HPP

#include <math.h>

using namespace std;

class Sigmoid{
public:
    static double calculate(double);
    static double derivative(double);
};
#endif
