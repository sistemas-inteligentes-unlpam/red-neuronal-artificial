#ifndef ACTIVATIONFUNCTION_HPP
#define ACTIVATIONFUNCTION_HPP

#include <iostream>
#include "Sigmoid.hpp"
#include "Sing.hpp"

using namespace std;

class ActivationFunction{
private:
public:
    ActivationFunction(int);
    double (*calculate)(double);
    double (*derivative)(double);
};
#endif
