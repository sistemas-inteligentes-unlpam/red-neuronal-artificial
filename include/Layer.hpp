#ifndef LAYER_HPP
#define LAYER_HPP

#include "Neuron.hpp"

class Layer{
private:
  double *inputs;
  double *outputs;
  int l_size;
  int i_size;
  Neuron **neuron;
  double *deltas;
  double theta;
  double* add_bias(double*);
public:
  Layer(int, int, int, int);
  double* get_inputs();
  double* get_outputs();
  int get_l_size();
  Neuron* get_neuron(int);
  double* get_deltas();
  double get_delta(int);
  double* evaluate(double*);
  void calculate_deltas_output_layer(double*, double);
  void calculate_deltas(Layer*, double);
  void summarize_layer(int);
  void print_l_size();
  void print_deltas();
  void print_neurons();
  void print_input();
  void print_output();
  void print_bias();
};
#endif
