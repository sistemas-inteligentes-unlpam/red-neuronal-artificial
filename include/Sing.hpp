#ifndef SING_HPP
#define SIGN_HPP

#include <math.h>

using namespace std;

class Sing{
public:
    static double calculate(double);
    static double derivative(double);
};
#endif
