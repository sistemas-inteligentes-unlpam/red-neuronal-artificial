#ifndef MULTILAYERPERCEPTRON_HPP
#define MULTILAYERPERCEPTRON_HPP

#include "Layer.hpp"

class MultilayerPerceptron{
private:
  int rn_size;
  Layer **layers;
  double eta;
  void backpropagation(double*, double*);
  double quadraticError(int, double*, double*);
  double* activate(double*);
  void shuffle(double**, int);
  void interchange(double**, double**);
public:
  MultilayerPerceptron(int, double, int*, int*);
  void train(int, int, double**, double**);
  void test(int, double**);
  void summarize_network();
  void take_snapshoot();
  void print_rn_size();
  void print_eta();
};
#endif
