
#ifndef NEURON_HPP
#define NEURON_HPP

#include <math.h>
#include <time.h>
#include <iostream>
#include "ActivationFunction.hpp"

using namespace std;

class Neuron{
private:
  double * weights;
  double * delta_w;
  int w_size;
  double sum(double*);
  ActivationFunction *af;
public:
  Neuron(int, int);
  int get_w_size();
  double * get_weights();
  double get_weight(int);
  double activate(double*);
  double derivative(double*);
  void calculate_delta_w_output_layer(double, double, double*);
  void calculate_delta_w(double, double, double*);
  double deltas_times_weights(double*);
  void update_weights();
  void summarize_neuron();
  void print_weights();
  void print_delta_w();
};
#endif
