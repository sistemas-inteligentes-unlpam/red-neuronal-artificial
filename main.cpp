#include "ActivationFunction.hpp"
#include "Layer.hpp"
#include "Neuron.hpp"
#include "MultilayerPerceptron.hpp"
#include <iostream>

using namespace std;
double** get_inputs(int);
double** get_desired(int);
int * get_function();
int* get_rn_arch();
double* set_inputs(char*);

int main (int argc, char *argv[]){
  int epoch = 1;
  
  int mu = 4;
  int *num_neurons = get_rn_arch();
  double **inputs = get_inputs(mu);
  double **desired = get_desired(mu);
  int *fn = get_function();

  MultilayerPerceptron *mlp = new MultilayerPerceptron(2,0.01,num_neurons, fn);

  //mlp->summarize_network();
 
  cout << "----------------------TRAINING----------------------" << endl;
  mlp->train(epoch, mu, desired, inputs);

  cout << "----------------------TEST----------------------" << endl;
  mlp->test(mu, inputs);


  //ActivationFunction af(2);
  //cout << af.calculate(-2.0) <<endl;

  //mlp->summarize_network();
}

double* set_inputs(int argc, char *argv[]){
  double *inputs = (double*) malloc(2*sizeof(double));
  for(int i=2; i<argc; i++){
    inputs[i] = atof(argv[i]);
  }
  return inputs;
}

double** get_inputs(int mu){
  int size = mu;
  double **inputs = (double**) malloc(size*sizeof(double));
  for(int i=0; i<size; i++){
    inputs[i] = (double*) malloc(2*sizeof(double));
  }
  inputs[0][0] = -1.0;
  inputs[0][1] = -1.0;
  inputs[1][0] = -1.0;
  inputs[1][1] = 1.0;
  inputs[2][0] = 1.0;
  inputs[2][1] = -1.0;
  inputs[3][0] = 1.0;
  inputs[3][1] = 1.0;

  return inputs;
}

double** get_desired(int mu){
  int size = mu;
  double **desired = (double**) malloc(size*sizeof(double));
  for(int i=0; i<size; i++){
    desired[i] = (double*) malloc(size*sizeof(double));
  }
  desired[0][0] = -1.0;
  desired[1][0] = 1.0;
  desired[2][0] = 1.0;
  desired[3][0] = -1.0;

  return desired;
}

int* get_function(){
  int *fn_array = (int*) malloc(2*sizeof(int));
  fn_array[0] = 1;
  fn_array[1] = 2;

  return fn_array;
}

int* get_rn_arch(){
  int *num_neurons = new int[2];
  num_neurons[0] = 2;
  num_neurons[1] = 3;
  num_neurons[2] = 1;
  return num_neurons;
}
